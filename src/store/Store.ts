import {configureStore} from "@reduxjs/toolkit";
import {postsApi} from "../reducers/posts/UsersApi";
import {categoriesApi} from "../reducers/categories/CategoriesApi";
import {productsApi} from "../reducers/products/ProductsApi";
import {regionApi} from "../reducers/adress/RegionsApi";
import {ordersApi} from "../reducers/orders/OrdersApi";
import loading from  "../reducers/loading/Loading"
import {discountApi} from "../reducers/discount/DiscountApi";
export const store = configureStore({
    reducer:{
        [postsApi.reducerPath]:postsApi.reducer,
        [categoriesApi.reducerPath]:categoriesApi.reducer,
        [productsApi.reducerPath]:productsApi.reducer,
        [regionApi.reducerPath]:regionApi.reducer,
        [ordersApi.reducerPath]:ordersApi.reducer,
        [discountApi.reducerPath]:discountApi.reducer,
        loading
    },
    middleware:(getDefaultMiddleware) => getDefaultMiddleware().concat([discountApi.middleware,ordersApi.middleware,regionApi.middleware,postsApi.middleware,categoriesApi.middleware,productsApi.middleware])
})
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
