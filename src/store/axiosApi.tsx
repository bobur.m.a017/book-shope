import {BaseQueryFn} from "@reduxjs/toolkit/query";
import axios, {AxiosError, AxiosRequestConfig} from "axios";
import {baseUrl as baseUrl2} from "../allFunc/AllFuncs";
import {toast} from "react-toastify";

export const saveToken = (data: any) => {
    localStorage.setItem("Authorization", "Bearer " + data?.access_token);
    localStorage.setItem("Refresh", "Bearer " + data?.refresh_token);
    if (data?.user_role) {
        localStorage.setItem("role", data?.user_role);
    }
}
export const getToken = () => {
    return localStorage.getItem("Authorization");
}
export const getRoleStorage = () => {
    return localStorage.getItem("role");
}
export const getRefreshToken = () => {
    return localStorage.getItem("Refresh");
}
export const headersToken = () => {
    return {Authorization: getToken()}
}
export const axiosBaseQuery =
    (
        {baseUrl}: { baseUrl: string } = {baseUrl: ''}
    ): BaseQueryFn<
        {
            url: string
            method: AxiosRequestConfig['method']
            data?: AxiosRequestConfig['data']
            params?: AxiosRequestConfig['params']
            headers?: AxiosRequestConfig['headers']
        },
        unknown,
        unknown
    > =>
        async ({url, method, data, params, headers}) => {
            try {
                const result = await axios({url: baseUrl + url, method, data, params, headers});
                console.log(result.data, "data");
                toast.success(result.data.text)
                return {data: result.data}
            } catch (axiosError) {
                let err = axiosError as AxiosError
                console.log(err, "errrrr");
                if (err.response?.status !== 401) {
                    // @ts-ignore
                    toast.error(`${err.response?.data?.text}` || err.message);
                    return {
                        error: {
                            status: err.response?.status,
                            data: err.response?.data || err.message,
                        },
                    }
                } else {
                    try {
                        const result = await axios({
                            url: baseUrl2() + "/token/refresh", headers: {
                                Authorization: getRefreshToken()
                            }
                        });
                        saveToken(result?.data);
                        const result2 = await axios({
                            url: baseUrl + url,
                            method,
                            data,
                            params,
                            headers: {...headers, Authorization: getToken()}
                        });
                        toast.success(result2.data.text)
                        return {data: result2.data}
                    } catch (axiosError2) {
                        let err2 = axiosError2 as AxiosError
                        if (err2?.response?.status === 401) {
                            // if (getRoleStorage() === "ROLE_USER") {
                            window.history.pushState("object or string", "Title", "/");
                            window.location.reload();
                            // } else {
                            localStorage.removeItem("Authorization");
                            localStorage.removeItem("Refresh");
                        } else {
                            console.log(err2?.response)
                            // @ts-ignore
                            toast.error(`${err2.response?.data?.text}` || err.message);
                            return {
                                error: {
                                    status: err2.response?.status,
                                    data: err2.response?.data || err2.message,
                                },
                            }
                        }
                        return {error: null};
                    }
                }
            }
        }
