import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICategory} from "../../pages/fromAdmin/CategoryDTO";

export const regionApi = createApi({
    reducerPath: "region/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addCategories"],
    endpoints: build => ({
        getRegions: build.query<any,any>({
            query: () => ({
                url: "/region",
                method:"GET",
                headers:headersToken()
            }),
        }),
    })
})
export const {useGetRegionsQuery} = regionApi
