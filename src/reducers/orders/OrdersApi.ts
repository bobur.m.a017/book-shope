import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICategory} from "../../pages/fromAdmin/CategoryDTO";
import {IOrder, IOrders} from "../../pages/fromAdmin/OrderDTO";

export const ordersApi = createApi({
    reducerPath: "orders/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addOrders"],
    endpoints: build => ({
        getOrders: build.query<IOrder[], any>({
            query: (params) => ({
                url: "/order/getAll",
                method: "GET",
                headers: headersToken()
            }),
            providesTags: ["addOrders"]
        }),
        getOrdersAdmin: build.query<IOrder[], any>({
            query: (params) => ({
                url: "/order/getAllAdmin",
                method: "GET",
                headers: headersToken()
            }),
            providesTags: ["addOrders"]
        }),
        editeOrders: build.mutation({
            query: ({data, params}) => ({
                url: "/order/" + data?.id,
                method: "PUT",
                params,
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrders"]
        }),
        deleteOrders: build.mutation<ICategory, ICategory>({
            query: (data) => ({
                url: "/order/" + data?.id,
                method: "DELETE",
                headers: headersToken(),
            }),
            invalidatesTags: ["addOrders"]
        }),
        addOrders: build.mutation({
            query: ({data, params}) => ({
                url: "/order/create",
                method: "POST",
                data,
                headers: headersToken(),
                // params:params
            }),
            invalidatesTags: ["addOrders"]
        }),
        checkCode: build.mutation({
            query: ({data, params}) => ({
                url: "/order/confirmation",
                method: "POST",
                data,
                headers: headersToken(),
                // params:params
            }),
            invalidatesTags: ["addOrders"]
        }),
        resetCheckCode: build.mutation({
            query: ({data, params}) => ({
                url: "/order/requestCodeAgain",
                method: "POST",
                data,
                headers: headersToken(),
                // params:params
            }),
            invalidatesTags: ["addOrders"]
        }),
    })
})
export const {useGetOrdersQuery,useCheckCodeMutation,useGetOrdersAdminQuery, useAddOrdersMutation, useEditeOrdersMutation, useDeleteOrdersMutation,useResetCheckCodeMutation} = ordersApi
