import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {IDiscount, IDiscountGet,} from "./DiscountDTO";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";

export const discountApi = createApi({
    reducerPath: "discount/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addDiscount"],
    endpoints: build => ({
        getDiscounts: build.query<IDiscountGet[], any>({
            query: (params) => ({
                url: "/discount",
                method:"GET",
                params,
                headers:headersToken()
            }),
            providesTags: ["addDiscount"]
        }),
        addDiscount: build.mutation<IDiscount, IDiscount>({
            query: (data) => ({
                url: "/discount",
                method: "POST",
                data: data,
                headers:headersToken()
            }),
            invalidatesTags: ["addDiscount"]
        }),
        editeDiscount: build.mutation<IDiscount, IDiscount>({
            query: (data) => ({
                url: "/discount/" + data.id,
                method: "PUT",
                data: data,
                headers:headersToken()
            }),
            invalidatesTags: ["addDiscount"]
        }),
        deleteDiscount: build.mutation<IDiscount, IDiscount>({
            query: (data) => ({
                url: "/discount/" + data.id,
                method: "DELETE",
                headers:headersToken()
            }),
            invalidatesTags: ["addDiscount"]
        }),
    })
})
export const {useGetDiscountsQuery, useAddDiscountMutation, useEditeDiscountMutation, useDeleteDiscountMutation} = discountApi
