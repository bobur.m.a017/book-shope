import {IProduct} from "../../pages/fromAdmin/ProductDTO";

export  interface IDiscount {
    id?:number,
    "books"?: number[],
    "percentage"?: number,
    "state"?: boolean,
    "validityPeriod"?:number
}
export  interface IDiscountGet {
    id?:number,
    "books"?: IProduct[],
    "percentage"?: number,
    "state"?: boolean,
    "validityPeriod"?:number
}
