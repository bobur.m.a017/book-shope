import {createSlice, PayloadAction} from "@reduxjs/toolkit";


interface ILoading {
    loading: boolean | undefined,
    send?:boolean
}

const initialState: ILoading = {
    loading: false,
    send: false,
}

const loadingSlice = createSlice({
    name: "loading",
    initialState,
    reducers: {
        loadingFunc(state, payload: PayloadAction<boolean | undefined>) {
            state.loading = payload.payload
        },
        smsIs(state, payload: PayloadAction<boolean | undefined>) {
            state.send = payload.payload
        }
    }
})
export const {loadingFunc,smsIs}=loadingSlice.actions
export default loadingSlice.reducer
