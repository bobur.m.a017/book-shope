import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICategory} from "../../pages/fromAdmin/CategoryDTO";

export const categoriesApi = createApi({
    reducerPath: "categories/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addCategories"],
    endpoints: build => ({
        getCategories: build.query<ICategory[],any>({
            query: (params) => ({
                url: "/category/get",
                method:"GET",
                params,
            }),
            providesTags: ["addCategories"]
        }),
        editeCategories: build.mutation({
            query: ({data, params}) => ({
                url: "/category/" + data?.id,
                method: "PUT",
                params,
                headers:headersToken(),
            }),
            invalidatesTags: ["addCategories"]
        }),
        deleteCategories: build.mutation<ICategory, ICategory>({
            query: (data) => ({
                url: "/category/" + data?.id,
                method: "DELETE",
                headers:headersToken(),
            }),
            invalidatesTags: ["addCategories"]
        }),
        addCategories: build.mutation({
            query: ({data,params}) => ({
                url: "/category",
                method: "POST",
                // data:params,
                headers:headersToken(),
                params:params
            }),
            invalidatesTags: ["addCategories"]
        }),

    })
})
export const {useGetCategoriesQuery, useAddCategoriesMutation, useEditeCategoriesMutation, useDeleteCategoriesMutation} = categoriesApi
