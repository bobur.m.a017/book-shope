// @ts-ignore
export interface ICategories {
    "id": number,
    "title": string,
    "body": string
}

export interface IPosts {
    posts: ICategories[]
}
