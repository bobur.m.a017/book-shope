import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {IPost,} from "./PostDTO";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ILoginToUse, ILoginUser, IStateMessage} from "../../pages/login/FromUser";

export const postsApi = createApi({
    reducerPath: "posts/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addPost"],
    endpoints: build => ({
        getPosts: build.query<any, any>({
            query: (params) => ({
                url: "/book",
                method:"GET",
                params,
                headers:headersToken()
            }),
            providesTags: ["addPost"]
        }),
        addPost: build.mutation<IPost, IPost>({
            query: (data) => ({
                url: "/posts",
                method: "POST",
                data: data
            }),
            invalidatesTags: ["addPost"]
        }),
        editePost: build.mutation<IPost, IPost>({
            query: (data) => ({
                url: "/posts/" + data.id,
                method: "PUT",
                data: data
            }),
            invalidatesTags: ["addPost"]
        }),
        deletePost: build.mutation<IPost, IPost>({
            query: (data) => ({
                url: "/posts/" + data.id,
                method: "DELETE",
            }),
            invalidatesTags: ["addPost"]
        }),
        loginUser: build.mutation<ILoginUser, string>({
            query: (data) => ({
                url: "/login",
                method: "POST",
                headers: {
                    'Content-type': 'application/x-www-form-urlencoded'
                },
                data:data
            }),
            invalidatesTags: ["addPost"]
        }),
        loginFromUser: build.mutation<ILoginUser, ILoginToUse>({
            query: (data) => ({
                url: "/user/checkSms",
                method: "POST",
                data:data
            }),
            invalidatesTags: ["addPost"]
        }),
        loginSendSms: build.mutation<IStateMessage, ILoginToUse>({
            query: (data) => ({
                url: "/user/sendSms",
                method: "POST",
                data:data
            }),
            invalidatesTags: ["addPost"]
        }),
    })
})
export const {useLoginUserMutation,useLoginFromUserMutation,useLoginSendSmsMutation} = postsApi;
