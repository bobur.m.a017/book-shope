import {createApi} from "@reduxjs/toolkit/query/react";
import {baseUrl} from "../../allFunc/AllFuncs";
import {axiosBaseQuery, headersToken} from "../../store/axiosApi";
import {ICategory} from "../../pages/fromAdmin/CategoryDTO";
import {IProduct} from "../../pages/fromAdmin/ProductDTO";

export const productsApi = createApi({
    reducerPath: "products/api",
    baseQuery: axiosBaseQuery({
        baseUrl: baseUrl(),
    }),
    tagTypes: ["addProducts"],
    endpoints: build => ({
        getProducts: build.query<IProduct[], any>({
            query: (params) => ({
                url: "/book/getAll",
                method:"GET",
                params:{categoryId:params},
            }),
            providesTags: ["addProducts"]
        }),
        getProductsSearch: build.query<IProduct[], any>({
            query: ({params}) => ({
                url: "/book/search",
                method:"GET",
                params,
            }),
            providesTags: ["addProducts"]
        }),
        getProductsRating: build.query<IProduct[], any>({
            query: (params) => ({
                url: "/book/ratingAll",
                method:"GET",
                params,
            }),
            providesTags: ["addProducts"]
        }),
        getProductsDiscount: build.query<IProduct[], any>({
            query: (params) => ({
                url: "/book/discountAll",
                method:"GET",
                params,
            }),
            providesTags: ["addProducts"]
        }),
        getProductsAdmin: build.query<any, any>({
            query: (params) => ({
                url: "/book/getAllAdmin",
                method:"GET",
                params,
                headers:headersToken(),
            }),
            providesTags: ["addProducts"]
        }),
        editeProducts: build.mutation({
            query: ({data}) => ({
                url: "/book/" + data?.id,
                method: "PUT",
                data,
                headers:headersToken(),
            }),
            invalidatesTags: ["addProducts"]
        }),
        restoreProducts: build.mutation({
            query: ({data}) => ({
                url: "/book/restore/" + data?.id,
                method: "PUT",
                headers:headersToken(),
            }),
            invalidatesTags: ["addProducts"]
        }),
        deleteProducts: build.mutation<IProduct, IProduct>({
            query: (data) => ({
                url: "/book/" + data?.id,
                method: "DELETE",
                headers:headersToken(),
            }),
            invalidatesTags: ["addProducts"]
        }),
        mainProducts: build.mutation({
            query: ({data,params}) => ({
                url: "/book/mainPicture/" + data?.id,
                method: "PUT",
                headers:headersToken(),
                params
            }),
            invalidatesTags: ["addProducts"]
        }),
        addProducts: build.mutation({
            query: ({data,params}) => ({
                url: "/book",
                method: "POST",
                data,
                headers:headersToken(),
                params:params
            }),
            invalidatesTags: ["addProducts"]
        }),
        addImages: build.mutation({
            query: ({data,id}) => ({
                url: "/book/addAttachment/"+id,
                method: "POST",
                data,
                headers:headersToken(),
                // params:params
            }),
            invalidatesTags: ["addProducts"]
        }),
        deleteImages: build.mutation({
            query: ({data,params}) => ({
                url: "/book/deleteAttachment/"+data.id,
                method: "DELETE",
                headers:headersToken(),
                params
            }),
            invalidatesTags: ["addProducts"]
        }),

    })
})
export const {useGetProductsQuery,useGetProductsSearchQuery,useGetProductsDiscountQuery,useGetProductsRatingQuery,useMainProductsMutation,useGetProductsAdminQuery,useRestoreProductsMutation, useAddProductsMutation, useEditeProductsMutation, useDeleteProductsMutation,useAddImagesMutation,useDeleteImagesMutation} = productsApi
