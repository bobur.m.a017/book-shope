import React, {useEffect, useState} from 'react';
import {getToken, saveToken} from "../store/axiosApi";
import './login/style.scss'
import {useLoginFromUserMutation, useLoginSendSmsMutation} from "../reducers/posts/UsersApi";
import {useAppDispatch, useAppSelector} from "../hooks/Hooks";
import {GrPowerReset} from "react-icons/gr";
import {ILoginToUse} from "./login/FromUser";
import {smsIs} from "../reducers/loading/Loading";
import {toast} from "react-toastify";
import {useNavigate} from "react-router-dom";

interface ILogin {
    children: JSX.Element
}

function UserLogin({children}: ILogin): JSX.Element {
    const [sendSmsToUser, dataResSendSms] = useLoginSendSmsMutation();
    const [checkSmsAndLogin, resCheckSms] = useLoginFromUserMutation();
    const navigateFunction = useNavigate();
    const dispatch = useAppDispatch();
    const sendSms = useAppSelector(state => state.loading.send);

    const [count, setCount] = useState(120);

    const [runCount, setRunCount] = useState(true);
    const stopAndRun = () => {
        // console.log(count)
        if (runCount) {
            setCount((count) => count - 1);
        }
    }

    useEffect(() => {
        const interval = setInterval(() => {
            stopAndRun();
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, [runCount]);

    useEffect(() => {
        if (count < 1) {
            setRunCount(false);
        }
    }, [count]);

    useEffect(() => {
        if (resCheckSms?.data?.access_token) {
            dispatch(smsIs(false));
            saveToken(resCheckSms?.data);
            navigateFunction("/order");
        } else {
            if (resCheckSms?.data?.access_token === null) {
                toast.error("Xatolik qaytadan uruning");
            }
        }
    }, [resCheckSms?.data?.access_token]);

    // useEffect(() => {
    //     if (dataResSendSms.isSuccess) {
    //         dispatch(smsIs(true));
    //     }
    // }, [dataResSendSms]);


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [data, setData] = useState<ILoginToUse>({phoneNumber: "+998"});
    const loginSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (sendSms) {
            checkSmsAndLogin({...data, phoneNumber: data.phoneNumber?.substring(4)} as ILoginToUse);
        } else {
            dispatch(smsIs(true));
            sendSmsToUser({...data, phoneNumber: data.phoneNumber?.substring(4)} as ILoginToUse);
            setData({...data, code: undefined});
            resetSekund();
        }
    }
    const resetSekund = () => {
        sendSmsToUser(data as ILoginToUse)
        setData({...data, code: undefined})
        setRunCount(true);
        setCount(120);
    }
    if (getToken()) {
        return children;
    }else {
        return (
        <div className={"flex p-2 h-[80vh]  justify-center items-center"}>
            <div className={"h-[50vh] w-full inLogin2 flex justify-center"}>
                <div className={"w-full h-full sm:w-[50vh] lg:w-1/3 inLogin flex justify-center items-center"}>
                    <div>
                        <div className={"text-center text-emerald-600 font-semibold"}>Avval tizimga kiring</div>
                        <div className={"flex justify-center mb-2"}>
                            <div className={"icon-logo-yk w-24 h-20 my-radius-buttons text-center"}></div>
                        </div>
                        <form onSubmit={loginSubmit}>
                            {sendSms ? <input type="number" name={"phoneNumber"}
                                              className={"my-border my-radius-buttons  text-center"}
                                              placeholder={"SMS kod"} value={data?.code || ''}
                                              onChange={(e) => setData({...data, code: parseInt(e.target.value)})}/> :
                                <input type="text" name={"phoneNumber"}
                                       className={"my-border my-radius-buttons  text-center"}
                                       placeholder={"Tel raqam"} value={data?.phoneNumber || ''}
                                       onChange={(e) => setData({...data, phoneNumber: e.target.value})}/>}

                            <br/>
                            <div className={"flex justify-center mt-3"}>
                                <button className={"my-bg-button px-2"}
                                        type={"submit"}>{!sendSms ? "Yuborish" : "Tasdiqlash"}</button>
                                {sendSms ?
                                    <button onClick={() => resetSekund()} type={'button'} className={"mx-2"}>
                                        <GrPowerReset/> {count}
                                    </button> : null}
                            </div>
                            <div className={"flex justify-center"}>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
    }
}

export default UserLogin;
