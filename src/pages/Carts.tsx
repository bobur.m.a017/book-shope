import React, {useEffect, useState} from 'react';
import SpecialCard from "./allBookCards/SpecialCard";
import {deleteCartsAll, getCartsLocalStorage, IAllCarts, ICart, pageNewurl, savePhone} from "./functions/Functions";
import {Button, Modal} from "react-bootstrap";
import {useGetRegionsQuery} from "../reducers/adress/RegionsApi";
import {useAddOrdersMutation, useCheckCodeMutation, useResetCheckCodeMutation} from "../reducers/orders/OrdersApi";
import {GrPowerReset} from "react-icons/gr";
import {useAppDispatch} from "../hooks/Hooks";
import {loadingFunc} from "../reducers/loading/Loading";

interface IUser {
    phoneNumber?: string;
    code?: string
}

function Carts() {
    const [cartsProduct, setCartsProduct] = useState<IAllCarts | null>(getCartsLocalStorage());
    const [user, setUser] = useState<IUser>();
    const [show, setShow] = useState(false);
    const [districts, setDistricts] = useState([]);
    const regions = useGetRegionsQuery("");
    const [addOrder, orderAddRes] = useAddOrdersMutation();
    const [checkCode, resCode] = useCheckCodeMutation()
    const [checkCode2, resCode2] = useResetCheckCodeMutation()
    const dispatch = useAppDispatch();

    const [count, setCount] = useState(120);
    const [runCount, setRunCount] = useState(true);
    const stopAndRun = () => {
        // console.log(count)
        if (runCount) {
            setCount((count) => count - 1);
        }
    }

    useEffect(() => {
        const interval = setInterval(() => {
            stopAndRun();
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, [runCount]);

    useEffect(() => {
        if (count < 1) {
            setRunCount(false);
        }
    }, [count]);

    useEffect(() => {
        if (orderAddRes.isLoading) {
            dispatch(loadingFunc(orderAddRes.isLoading))
        }
    }, [orderAddRes.isLoading]);

    useEffect(() => {
        if (orderAddRes.isSuccess) {
            handleShow();
        }
    }, [orderAddRes.isSuccess]);

    useEffect(() => {
        if (orderAddRes.isError) {
            dispatch(loadingFunc(false))
        }
    }, [orderAddRes.isError]);

    useEffect(() => {
        if (orderAddRes.isSuccess || resCode2.isSuccess) {
            deleteCartsAll()
            savePhone(user?.phoneNumber||"");
            dispatch(loadingFunc(false))
            pageNewurl()
        }
    }, [resCode.isSuccess,resCode2.isSuccess]);
    const handleClose = () => {
        setCount(0);
        setRunCount(false);
        setShow(false);
        dispatch(loadingFunc(false))
    }
    const handleShow = () => {
        setCount(120)
        setRunCount(true)
        setShow(true);
    };
    const totalSum = () => {
        let sum = 0;
        cartsProduct?.carts.forEach((item) =>
            sum += (item?.book?.price! * item.count)
        )
        return sum;
    }
    const totalCount = () => {
        let sum = 0;
        cartsProduct?.carts.forEach((item) =>
            sum += item.count
        )
        return sum;
    }
    const getDistricts = (e: React.ChangeEvent<HTMLSelectElement>) => {
        let dis = regions?.data?.filter((item: any) => item.id === parseInt(e?.target.value));
        setDistricts(dis[0]?.districtList);
    }
    const getCartsFun = () => {
        setCartsProduct(getCartsLocalStorage())
    }
    const submitOrder = (e: any) => {
        e.preventDefault();
        let data = {
            bookOrderList: cartsProduct?.carts.map((cart => {
                return {
                    "bookId": cart.book.id,
                    "count": cart.count
                }
            })),
            "districtId": e.target?.district.value,
            "name": e.target.name.value,
            "phoneNumber": e.target?.phoneNumber.value
        }
        addOrder({data})
        console.log(data)
        // handleShow();
        setUser({...user,phoneNumber:e.target?.phoneNumber.value})
    }
    const resetSekund = () => {
        checkCode2({data: user})
        setRunCount(true);
        setCount(2);
    }
    const sendCode = () => {
        checkCode({data: user})
    }

    return (
        <div>
            <h1 className={"text-center mt-2 my-color"}>Savatcha</h1>
            <div
                className={"place-items-center grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 px-2"}>
                {cartsProduct?.carts?.map((item: ICart, index: number) =>
                    <div key={index}>
                        <SpecialCard specialProductDto={item.book} count={item.count} getCartsFun={getCartsFun}/>
                    </div>
                )}
            </div>
            <div className={"ml-2"}> {totalCount()} ta kitob Jami: {totalSum()} so‘m</div>
            <form onSubmit={submitOrder}>
                <div className={"grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 p-5"}>
                    <div className={"input-label sm:mt-3"}>Yetkazib berish manzilini kiriting</div>
                    <select className={"input-label pr-2 sm:mt-3"} onChange={(e) => getDistricts(e)} >
                        <option value={''}>Viloyatni tanlang</option>
                        {
                            regions.data?.map((item: any, index: number) =>
                                <option value={item.id} key={index}>{item?.name}</option>
                            )
                        }
                    </select>
                    <select className={"input-label pr-2 sm:mt-3"} name={"district"} >
                        <option value={1}>Tumanni tanlang</option>
                        {
                            districts?.map((item: any, index: number) =>
                                <option value={item.id} key={index}>{item?.name}</option>
                            )
                        }
                    </select>
                    <input className={"input-style sm:mt-3"} placeholder={"Manzilni kiriting"} required
                           name={"street"}/>
                    <div className={"input-label md:mt-3 sm:mt-3"}>Ismingizni kiriting</div>
                    <input className={"input-style md:col-span-3 md:mt-3 sm:mt-3"} placeholder={"Ismingiz"} required
                           name={"name"}/>
                    <div className={"input-label md:mt-3 sm:mt-3"}>Telefon raqamingizni kiriting</div>
                    <input className={"input-style md:col-span-3 md:mt-3 sm:mt-3"} placeholder={"+998"}
                           defaultValue={"998"} required
                           name={"phoneNumber"}/>
                </div>
                <div className={"flex justify-center"}>
                    <button className={"my-bg-button text-center px-3"} type={'submit'}>Buyurtma berish</button>
                </div>
            </form>
            <br/>
            <Modal show={show}>
                <Modal.Header>
                    <Modal.Title>Buyurtmani rasmiylashtirish uchun tadiqlash kodini kiriting. </Modal.Title>
                </Modal.Header>
                <Modal.Body className={"flex justify-center"}>
                    <div>
                        <input className={"all input-style md:col-span-3 md:mt-3 sm:mt-3"}
                               placeholder={"Tasdiqlash kodi"} value={user?.code || ''}
                               onChange={(e) => setUser({...user, code: e.target.value})}/>
                        <div>
                            <div>{count}</div>
                            <div>
                                <button onClick={() => resetSekund()}><GrPowerReset/></button>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                    <Button variant="primary" onClick={sendCode}>
                        Tasdiqlash
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Carts;
