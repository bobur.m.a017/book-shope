import React, {useState} from 'react';
import {Carousel} from "react-bootstrap";
import {TiShoppingCart} from "react-icons/ti";

function MainCaroucel() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex: any, e: any) => {
        setIndex(selectedIndex);
    };
    return (
        <div>
            <Carousel activeIndex={index} onSelect={handleSelect}>
                <Carousel.Item style={{position: 'relative'}}>
                    <div style={{backgroundImage:`url(https://avatars.dzeninfra.ru/get-zen-logos/200214/pub_5f50f2cbec366f51f6c053fe_612a1c4b0e2b051ebef10a80/xxh)`}}
                        className="img-carousel2 d-block"
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption2'}>
                        <div className={"px-10 text-center"}>
                            <h5 className={"bg-amber-50 px-3 rounded-lg"}>First slide label</h5>
                            <div className={"flex justify-center"}>
                                <button className={"my-w-button flex items-center justify-center"}><TiShoppingCart
                                    size={30} color={"#6C5DD4"}/> <span
                                    className={" text-center"}>Savatga qo‘shish</span></button>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{position: 'relative'}}>
                    <div style={{backgroundImage:`url(https://avatars.dzeninfra.ru/get-zen-logos/200214/pub_5f50f2cbec366f51f6c053fe_612a1c4b0e2b051ebef10a80/xxh)`}}
                         className="img-carousel2 d-block"
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption2'}>
                        <div className={"px-10 text-center"}>
                            <h5 className={"bg-amber-50 px-3 rounded-lg"}>First slide label</h5>
                            <div className={"flex justify-center"}>
                                <button className={"my-w-button flex items-center justify-center"}><TiShoppingCart
                                    size={30} color={"#6C5DD4"}/> <span
                                    className={" text-center"}>Savatga qo‘shish</span></button>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{position: 'relative'}}>
                    <div style={{backgroundImage:`url(https://avatars.dzeninfra.ru/get-zen-logos/200214/pub_5f50f2cbec366f51f6c053fe_612a1c4b0e2b051ebef10a80/xxh)`}}
                         className="img-carousel2 d-block"
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption2'}>
                        <div className={"px-10 text-center"}>
                            <h5 className={"bg-amber-50 px-3 rounded-lg"}>First slide label</h5>
                            <div className={"flex justify-center"}>
                                <button className={"my-w-button flex items-center justify-center"}><TiShoppingCart
                                    size={30} color={"#6C5DD4"}/> <span
                                    className={" text-center"}>Savatga qo‘shish</span></button>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    );
}

export default MainCaroucel;
