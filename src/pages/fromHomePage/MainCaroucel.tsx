import React, {useState} from 'react';
import {Carousel} from "react-bootstrap";
import img1 from '../../images/book-shope1.jpg'
import img2 from '../../images/book-shope2.jpg'
import img3 from '../../images/book-shope3.jpg'

function MainCaroucel() {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex: any, e: any) => {
        setIndex(selectedIndex);
    };
    return (
        <div className={"shadow-2xl rounded-2xl"}>
            <Carousel activeIndex={index} onSelect={handleSelect} bsPrefix={'carousel'}>
                <Carousel.Item className={"position-relative"}>
                    <div style={{backgroundImage:`url(${img1})`}}
                         className="img-carousel2 d-block"
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption'}>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item className={"position-relative"}>
                    <div style={{backgroundImage:`url(${img2})`}}
                         className="img-carousel2 "
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption'}>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item className={"position-relative"}>
                    <div style={{backgroundImage:`url(${img3})`}}
                         className="img-carousel2 d-block"
                    />
                    <Carousel.Caption bsPrefix={'carousel-caption'}>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    );
}

export default MainCaroucel;
