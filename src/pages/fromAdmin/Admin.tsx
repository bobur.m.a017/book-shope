import React from 'react';
import {Link, Route, Routes} from "react-router-dom";
import Orders2 from "./Orders";
import Categories from "./Categories";
import Products from "./Products";
import Discounts from "../Discounts";
import "./adminStyle.scss"
import {BiCategory} from "react-icons/bi";
import {CiDeliveryTruck} from "react-icons/ci";
import {GiOpenBook} from "react-icons/gi";
import {TbShoppingCartDiscount} from "react-icons/tb";
function Admin():JSX.Element {
    return (
        <div>
            <div className={"my-navbar shadow-2xl w-full hidden md:block"}>
                <div className={"position-absolute pl-3 w-full"}>
                    <Link to={"/admin/categories"} className={"no-link p-2"}>
                        <div className={"item-nav flex items-center w-[100px]"}>
                            <BiCategory size={25} color={"#6C5DD4"}/>
                            <span className={"mx-3"}>Bo'limlar</span>
                        </div>
                    </Link>
                    <Link to={"/admin/orders"} className={"no-link p-2"}>
                        <div className={"flex items-center item-nav"}>
                            <CiDeliveryTruck size={25} color={"#6C5DD4"}/>
                            <span className={"mx-3"}>Buyurtma</span>
                        </div>
                    </Link>
                    <Link to={"/admin/products"} className={"no-link p-2"}>
                        <div className={"flex items-center item-nav"}>
                            <GiOpenBook size={25} color={"#6C5DD4"}/>
                            <span className={"mx-3"}>Kitoblar</span>
                        </div>
                    </Link>
                    <Link to={"/admin/discounts"} className={"no-link p-2"}>
                        <div className={"flex items-center item-nav"}>
                            <TbShoppingCartDiscount size={25} color={"#6C5DD4"}/>
                            <span className={"mx-3"}>Chegirmalar</span>
                        </div>
                    </Link>
                </div>
            </div>
            <div className={"md:ml-14 mt-0 mb-10 md:mb-0 md:mt-20 "}>
                <Routes>
                    <Route path={"/orders"} element={<Orders2/>}/>
                    <Route path={"/categories"} element={<Categories/>}/>
                    <Route path={"/products"} element={<Products/>}/>
                    <Route path={"/discounts"} element={<Discounts/>}/>
                </Routes>
            </div>
        </div>
    );
}

export default Admin;
