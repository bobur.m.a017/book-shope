import React, {useEffect, useState} from 'react';
import {
    useAddCategoriesMutation, useDeleteCategoriesMutation,
    useEditeCategoriesMutation,
    useGetCategoriesQuery
} from "../../reducers/categories/CategoriesApi";
import {Button, Card, Form, Modal} from "react-bootstrap";
import {FiEdit} from "react-icons/fi";
import {MdDeleteForever} from "react-icons/md";
import {ICategory} from "./CategoryDTO";
import {ERole, getRoleStorge} from "../functions/Functions";
import {Link} from "react-router-dom";
import {getToken} from "../../store/axiosApi";

function Categories() {
    const [show, setShow] = useState(false);
    const [numberRender, setNumberRender] = useState(0);
    const [params, setParams] = useState({name: ''});
    const [categoryState, setCategoryState] = useState<ICategory | undefined>();
    const handleClose = () => {
        setShow(false);
    }
    const handleShow = (num: number, data: ICategory | undefined) => {
        setNumberRender(num);
        setCategoryState(data);
        setShow(true);
    }

    const categories = useGetCategoriesQuery("")
    const [categoryFunc, categoryResult] = useAddCategoriesMutation();
    const [editeCategory, editeResult] = useEditeCategoriesMutation();
    const [deleteCategory, deleteResult] = useDeleteCategoriesMutation();

    useEffect(() => {
        handleClose();
    }, [deleteResult]);

    const postCategory = () => {
        if (categoryState?.id) {
            editeCategory({data: categoryState, params: {name: categoryState?.name}});
        } else {
            categoryFunc({categoryState, params: {name: categoryState?.name}});
        }
        handleClose();
    }
    const renderCategory = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>{categoryState?.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Control name={"name"} value={categoryState?.name || ''} type={"text"}
                                  onChange={(e) => setCategoryState({...categoryState, name: e.target.value})}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                    <Button variant="primary" onClick={postCategory}>
                        Saqlash
                    </Button>
                </Modal.Footer>
            </>
        )
    }
    const renderDelete = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>{categoryState?.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>Siz shu bo'limni o'chirmoqchimisiz ?</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yo'q
                    </Button>
                    <Button variant="primary" onClick={() => deleteCategory(categoryState as ICategory)}>
                        Ha
                    </Button>
                </Modal.Footer>
            </>
        )
    }
    return (
        <div>
            <div>
                {getToken() ? <button className={"my-bg-button ml-2 mt-2"} onClick={() => handleShow(0, undefined)}>Bo'lim qo'shish
                </button>:null}
            </div>
            <div className={"px-2"}>
                {categories.data?.map((item: ICategory, index: number) =>
                    <Card key={index + 1} className={"my-2 shadow-sm shadow-black p-2 my-color"}>
                        <div className={"flex justify-between"}>
                            <Link to={"/product/" + item.id}
                                  className={"inline-block no-link-color max-w-[230px] sm:max-w-full"}>{item?.name}</Link>
                            {getRoleStorge() === ERole.Admin ? <div>
                                <button onClick={() => handleShow(0, item)}><FiEdit color={"blue"} className={"mx-2"}/>
                                </button>
                                <button onClick={() => handleShow(1, item)}><MdDeleteForever color={"red"}/></button>

                            </div> : null}
                        </div>
                    </Card>
                )}
            </div>
            <Modal show={show} onHide={handleClose}>
                {numberRender === 0 ? renderCategory() : null}
                {numberRender === 1 ? renderDelete() : null}
            </Modal>
        </div>
    );
}

export default Categories;
