import {IProduct} from "./ProductDTO";

export interface IOrder {
    countBook:number
    createDate:number
    district:string
    id:number
    paid: boolean
    region: string
    status:string
    street?: null|string
    totalSum: number
    updateDate:number
    bookList:IProduct[]
}
export interface IOrders {

}
