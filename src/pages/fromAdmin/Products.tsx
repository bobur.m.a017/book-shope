import React, {useEffect, useState} from 'react';
import {Button, Card, Carousel, Form, FormCheck, Modal} from "react-bootstrap";
import {FiEdit} from "react-icons/fi";
import {MdDeleteForever} from "react-icons/md";
import imaa from "../../images/img.png";
import {
    useAddImagesMutation,
    useAddProductsMutation, useDeleteImagesMutation, useDeleteProductsMutation,
    useEditeProductsMutation, useGetProductsAdminQuery, useMainProductsMutation, useRestoreProductsMutation
} from "../../reducers/products/ProductsApi";
import {IProduct} from "./ProductDTO";
import {useGetCategoriesQuery} from "../../reducers/categories/CategoriesApi";
import {ICategory} from "./CategoryDTO";
import SpecialCard from "../components/SpecialCard";
import {imageUrl} from "../../allFunc/AllFuncs";
import {TbShoppingCartDiscount} from "react-icons/tb";
import {IDiscount} from "../../reducers/discount/DiscountDTO";
import {useAddDiscountMutation} from "../../reducers/discount/DiscountApi";
import {useNavigate} from "react-router-dom";

function Products() {
    const prodObj: IProduct = {
        "author": "",
        "categories": [],
        "description": "",
        "name": "",
        "oldPrice": 0,
        "price": 0
    };
    const [show, setShow] = useState(false);
    const [discount, setDiscount] = useState<IDiscount>();
    const [files, setFiles] = useState<any>();
    const [numberRender, setNumberRender] = useState(0);
    const [params, setParams] = useState({name: ''});
    const  history = useNavigate();
    const [productsState, setProductsState] = useState<IProduct | undefined>();
    const products = useGetProductsAdminQuery("");
    const categories = useGetCategoriesQuery("");
    const [addProduct, resultsAdd] = useAddProductsMutation();
    const [editeProduct, resultsEdite] = useEditeProductsMutation();
    const [addImagApi, imageRes] = useAddImagesMutation()
    const [deleteImagApi, imageDelRes] = useDeleteImagesMutation()
    const [mainImagApi, mainImgRes] = useMainProductsMutation()
    const [deleteProductApi, deleteProdRes] = useDeleteProductsMutation()
    const [restoreProductApi, restoreProdRes] = useRestoreProductsMutation()
    const [addDiscount, resDiscountAdd] = useAddDiscountMutation()
    const handleClose = () => {
        setShow(false);
    }
    const handleShow = (num: number, data: IProduct | undefined) => {
        if (num === 0 && !data) {
            setProductsState({
                ...prodObj, categories: categories?.data?.map((category) => {
                    return {...category, checked: false};
                })
            });
            // setProductsState()
        } else if (num === 2) {
            setProductsState(data);
        } else {
            let categor = categories?.data?.map((item) => {
                if (data?.categories?.some(cat => cat.id === item.id)) {
                    return {...item, checked: true}
                } else {
                    return {...item, checked: false}
                }
            })
            setProductsState({...data, categories: categor});
        }
        setNumberRender(num);
        setShow(true);
    }

    useEffect(() => {
        handleClose();
    }, [products.data, resultsAdd, resultsEdite, imageRes, imageDelRes]);

    const postProduct = (e: React.FormEvent) => {
        e.preventDefault();
        if (productsState?.id) {
            // console.log({data: {...productsState, categories: productsState?.categories?.filter(item => item.checked === true)}}, "productsState edite");
            editeProduct({
                data: {
                    ...productsState,
                    categories: productsState?.categories?.filter(item => item.checked === true)
                }
            });
        } else {
            addProduct({
                data: {...productsState, categories: productsState?.categories?.filter(item => item.checked === true)},
                params: undefined
            });
        }
        // handleClose();
    }
    const checkedCategory = (item: ICategory, e: React.ChangeEvent<HTMLInputElement>) => {
        let categories1 = productsState?.categories?.map((category) => {
            if (item.id === category.id) {
                return {...category, checked: e.target.checked}
            } else {
                return category
            }
        });
        setProductsState({...productsState, categories: categories1})
    }
    const renderCategory = () => {
        return (
            <>
                <Form onSubmit={postProduct}>
                    <Modal.Header closeButton>
                        <Modal.Title>{productsState?.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Label>Nomi</Form.Label>
                        <Form.Control name={"name"} value={productsState?.name || ''} type={"text"} size={"sm"} required
                                      onChange={(e) => setProductsState({...productsState, name: e.target.value})}/>
                        <Form.Label>Muallifi</Form.Label>
                        <Form.Control name={"author"} value={productsState?.author || ''} type={"text"} size={"sm"}
                                      required
                                      onChange={(e) => setProductsState({...productsState, author: e.target.value})}/>
                        <Form.Label>Narxi</Form.Label>
                        <Form.Control name={"price"} value={productsState?.price || ''} type={"number"} size={"sm"}
                                      required
                                      onChange={(e) => setProductsState({
                                          ...productsState,
                                          price: parseInt(e.target.value)
                                      })}/>
                        <Form.Label>Eski narxi</Form.Label>
                        <Form.Control name={"oldPrice"} value={productsState?.oldPrice || ''} type={"number"}
                                      size={"sm"}
                                      onChange={(e) => setProductsState({
                                          ...productsState,
                                          oldPrice: parseInt(e.target.value)
                                      })}/>
                        <Form.Label>Izoh</Form.Label>
                        <Form.Control name={"description"} value={productsState?.description || ''} type={"text"}
                                      as="textarea"
                                      size={"sm"} required
                                      onChange={(e) => setProductsState({
                                          ...productsState,
                                          description: e.target.value
                                      })}/>
                        <br/>
                        <div>Tegishli bo'limlarni tanlang</div>
                        {
                            productsState?.categories?.map((category, index) =>
                                <FormCheck type={"checkbox"} checked={category?.checked}
                                           onChange={(event) => checkedCategory(category, event)}
                                           label={category.name} key={index}/>
                            )
                        }

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Yopish
                        </Button>
                        <Button variant="primary" type={"submit"}>
                            Saqlash
                        </Button>
                    </Modal.Footer>
                </Form>
            </>
        )
    }
    const renderDelete = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>{productsState?.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>Siz shu bo'limni o'chirmoqchimisiz ?</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yo'q
                    </Button>
                    <Button variant="primary" onClick={() => deleteProductApi(productsState as IProduct)}>
                        Ha
                    </Button>
                </Modal.Footer>
            </>
        )
    }

    const changeInputFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFiles(e?.currentTarget?.files?.item(0));
    }
    const addImage = () => {
        let formData = new FormData();
        formData.append("file", files);
        addImagApi({data: formData, id: productsState?.id});
    }
    const renderImageAdd = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>Rasmlar</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className={"flex"}>
                        {productsState?.attachments?.map((id, index) =>
                            <div key={index} className={"m-2"} style={{border: '1px solid green'}}>
                                <div className={"cursor-pointer"} onClick={() => deleteImagApi({
                                    data: {id: productsState.id},
                                    params: {attachmentId: id}
                                })}>
                                    <MdDeleteForever
                                        color={"red"} size={30}/>
                                </div>
                                <img src={imageUrl(id)} alt="" className={"w-[100px]"} height={200} width={200}/>
                                <div className={"cursor-pointer"} onClick={() => mainImagApi({
                                    data: {id: productsState.id},
                                    params: {attachmentId: id}
                                })}>
                                    <Form.Check type={"radio"}/>
                                </div>
                            </div>
                        )}
                    </div>
                    <div>
                        <Form.Control type={"file"} name={'images'} onChange={changeInputFile}
                                      accept='image/png, image/jpeg'/>
                    </div>
                    <div className={"flex justify-center mt-5"}>
                        {files && <img src={URL.createObjectURL(files)} alt="" width={250}/>}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Ortga
                    </Button>
                    <Button variant="primary" onClick={addImage}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </>
        )
    }
    const checkedEvent = (id: number | undefined, e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            let list: number[] = [];
            if (discount?.books) {
                if (discount?.books?.some(item => item === id)) {

                } else {
                    list = discount.books
                    list?.push(id!)
                }
            } else {
                list.push(id!)
            }
            setDiscount({...discount, books: list})
        } else {
            setDiscount({...discount, books: discount?.books?.filter(bookId => bookId !== id)})
        }
    }
    const sendDiscount = (e: any) => {
        e.preventDefault();
        addDiscount({
            ...discount,
            percentage: e.target.percentage.value,
            validityPeriod: new Date(e.target.validityPeriod.value).getTime()
        } as IDiscount);
        handleClose();
    }
    const discountModal = () => {
        return (
            <>
                <Modal.Header closeButton>
                    <Modal.Title>Chegirma qo'shish</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className={"flex"}>
                    </div>
                    <div>
                        <Form id={"discount"} onSubmit={sendDiscount}>
                            <Form.Label>Chegirma foizi</Form.Label>
                            <Form.Control type={"number"} step={"0.01"} name={'percentage'} required={true}/>
                            <Form.Label>Chegirma tugash vaqti</Form.Label>
                            <Form.Control type={"date"} name={'validityPeriod'} required={true}/>
                        </Form>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Ortga
                    </Button>
                    <Button variant="primary" type={'submit'} form={"discount"}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </>
        )

    }

    return (
        <div>
            <div>
                <button className={"my-bg-button ml-2 mt-2"} onClick={() => handleShow(0, undefined)}>Kitob qo'shish
                </button>
                <button className={"my-bg-button ml-2 mt-2"} onClick={() => handleShow(4, undefined)}>Chegirma qo'shish
                </button>
            </div>
            <div className={"px-2 my-scroll-auto"}>
                {products.data?.map((item: IProduct, index: number) =>
                    <Card key={index + 1} className={"my-2 shadow-sm shadow-black p-2 my-color min-w-[400px]"}>
                        <div className={"flex"}>
                            <div className={"w-[150px] sm:w-[250px] "}>
                                <Carousel onClick={() => handleShow(3, item)}>
                                    {
                                        item?.attachments && item?.attachments?.length > 0 ? item.attachments?.map((id) =>
                                            <Carousel.Item key={id}>
                                                <div className={"special-card-img2 w-full h-[100px]"}
                                                     style={{backgroundImage: `url(${imageUrl(id)})`}}>
                                                </div>

                                            </Carousel.Item>) : <div style={{color:'green'}}>Rasm qo'shish</div>}
                                </Carousel>
                            </div>
                            <div>
                                <Form.Check type={'checkbox'} checked={item.checked} name={'checked'} className={"mx-2"}
                                            onChange={(e) => checkedEvent(item.id, e)}/>
                            </div>
                            <div className={"flex justify-between w-full pl-5"}>
                                <div className={"inline-block max-w-[230px] sm:max-w-full"}
                                     onClick={() => handleShow(2, item)}>{item?.name}</div>
                                <div>
                                    <div className={"flex justify-between"}>
                                        <button onClick={() => restoreProductApi({data: item})}>{item.stateName}
                                        </button>
                                        <button onClick={() => handleShow(0, item)}><TbShoppingCartDiscount size={25}
                                                                                                            color={"#c47e03"}
                                                                                                            className={"mx-2"}/>
                                        </button>
                                        <button onClick={() => handleShow(0, item)}><FiEdit color={"blue"}
                                                                                            className={"mx-2"}/>
                                        </button>
                                        <button onClick={() => handleShow(1, item)}><MdDeleteForever color={"red"}/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Card>
                )}
            </div>
            <Modal show={show} onHide={handleClose}>
                {numberRender === 0 ? renderCategory() : null}
                {numberRender === 1 ? renderDelete() : null}
                {numberRender === 2 ? <div className={"my-scroll grid h-full sm:h-full m-2"}>
                    <SpecialCard specialProductDto={productsState as IProduct}/>
                    <div></div>
                </div> : null}
                {numberRender === 3 ? renderImageAdd() : null}
                {numberRender === 4 ? discountModal() : null}
            </Modal>
        </div>
    );
}

export default Products;
