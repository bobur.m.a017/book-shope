import React, {useEffect} from 'react';
import {Table} from "react-bootstrap";
import {useGetOrdersAdminQuery} from "../../reducers/orders/OrdersApi";
import {saveToken} from "../../store/axiosApi";
import {useLoginUserMutation} from "../../reducers/posts/UsersApi";

function Orders() {
    const [loginUser, userLogin] = useLoginUserMutation()
    const orders = useGetOrdersAdminQuery("")

    useEffect(() => {
        if (userLogin.data) {
            saveToken(userLogin.data);
        }
    }, [userLogin.data]);

    useEffect(() => {
        if (userLogin.data) {

        }
    }, [orders]);

    const styleStatusButton = (name: string | null | undefined): string => {
        if (name === "To'landi") {
            return "#9BFECC"
        } else if (name === "Yetkazib berildi") {
            return "#9FA2FF"
        } else if (name === "Bekor qilindi") {
            return "#FF9494"
        } else {
            return "#C5EBFF"
        }
    }
    const styleStatusButtonText = (name: string | null | undefined): string => {
        if (name === "To'landi") {
            return "#369F6A"
        } else if (name === "Yetkazib berildi") {
            return "#392690"
        } else if (name === "Bekor qilindi") {
            return "#801414"
        } else {
            return "#C5EBFF"
        }
    }
    const styleTableBGx = (name: string | null | undefined): string => {
        if (name === "To'landi") {
            return "#DCFFEE"
        } else if (name === "Yetkazib berildi") {
            return "#E9E9FF"
        } else if (name === "Bekor qilindi") {
            return "#FFE9E9"
        } else {
            return "#C5EBFF"
        }
    }
    return (
        <div>
            <h1 className={"my-color text-center"}>Buyurtmalar</h1>
            <div className={"p-3 my-scroll-auto"}>
                <Table size={"sm"} className={"text-center"}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Kitoblar soni</th>
                        <th>Manzil</th>
                        <th>Summa</th>
                        <th>Holati</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        orders.data?.map((item, index) =>
                            <tr key={index} className={`place-items-center`}
                                style={{backgroundColor: styleTableBGx(item.status)}}>
                                <td>{index + 1}</td>
                                <td>{item.countBook}</td>
                                <td>{item.district} {item.street}</td>
                                <td>{item.totalSum}</td>
                                <td>{item.status}</td>
                                <td>
                                    {item.status !== "TO'LOV QILINMADI" ? <button className={"p-2"} style={{
                                        backgroundColor: styleStatusButton(item.status),
                                        color: styleStatusButtonText(item.status),
                                        borderRadius: 8
                                    }}>{item.status}</button> : <div className={"flex"}>
                                        <button className={"mx-2 p-2"}
                                                style={{backgroundColor: "#00F20A", borderRadius: 8}}>TO'LOV QILINMADI
                                        </button>
                                        <button style={{backgroundColor: "#FF1212", borderRadius: 8}}
                                                className={"p-2"}>Bekor qilish
                                        </button>
                                    </div>}
                                </td>
                            </tr>
                        )
                    }
                    </tbody>
                </Table>
            </div>
        </div>
    );
}

export default Orders;
