import {ICategory} from "./CategoryDTO";

export interface IProduct {
    id?: number,
    author?: string,
    categories?: ICategory[],
    "delete"?: true | false,
    "description"?: string,
    "name"?: string,
    "oldPrice"?: number,
    "price"?: number,
    "rating"?: number,
    "state"?: true | false,
    "attachments"?: number[]| null | undefined,
    "discount"?: number | null | undefined,
    stateName?:string,
    checked?:boolean
}
