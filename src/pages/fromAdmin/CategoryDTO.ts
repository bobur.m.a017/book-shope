export interface ICategory {
    id?:number|undefined,
    name?:string|undefined,
    checked?:true|false|undefined
}
