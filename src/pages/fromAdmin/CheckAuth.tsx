import React, { ReactChildren} from 'react';
import {getToken} from "../../store/axiosApi";
import {Navigate} from "react-router-dom";
interface IChildComponent {
    children:JSX.Element
}
function CheckAuth({children}:IChildComponent): JSX.Element {
    if (!getToken()){
        return <Navigate to={"/"}/>
    }
    return children;
}

export default CheckAuth;
