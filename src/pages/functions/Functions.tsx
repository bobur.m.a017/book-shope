import {Link, useNavigate} from "react-router-dom";
import {AiOutlineSearch} from "react-icons/ai";
import {TiShoppingCart} from "react-icons/ti";
import {BsFillBagHeartFill} from "react-icons/bs";
import React from "react";
import {IProduct} from "../fromAdmin/ProductDTO";
import {toast} from "react-toastify";

export interface ICart {
    book: IProduct
    count: number,
}

export interface IAllCarts {
    carts: ICart[],
}

export const getLinks = (text: string | null) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const history = useNavigate();
    const checkOrder = () => {
        if (getRoleStorge()) {
            history("/order")
        } else {
            toast.error("Avval ro'yxatdan o'ting");
        }
    }

    if (text === "ROLE_ADMIN") {
        return (
            <ul className={"flex justify-around items-center place-items-center text-center pl-0 mb-0"}>
                <li className={"p-2"}>
                    <Link to={"/"} className={"no-link-color"}>
                        <div className={"icon-logo-yk w-10 h-10"}></div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color"}>
                    <Link to={"/admin/products"} className={"no-link-color"}>
                        <div className={"w-100 flex justify-center"}><AiOutlineSearch size={25} color={"#6C5DD4"}/>
                        </div>
                        <div>Mahsulotlar</div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color"}>
                    <Link to={"/admin/categories"} className={"no-link-color"}>
                        <div className={"w-100 flex justify-center"}><TiShoppingCart size={25} color={"#6C5DD4"}/>
                        </div>
                        <div>Bo'limlar</div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color"}>
                    <Link to={"/admin/orders"} className={"no-link-color"}>
                        <div className={"w-100 flex justify-center"}><BsFillBagHeartFill size={25} color={"#6C5DD4"}/>
                        </div>
                        <div>Buyurtmalar</div>
                    </Link>
                </li>
            </ul>
        )
    } else {
        return (
            <ul className={"flex justify-around items-center place-items-center text-center pl-0 mb-0"}>
                <li className={"p-2"}>
                    <Link to={"/"} className={"no-link-color"}>
                        <div className={"icon-logo-yk w-10 h-10"}></div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color"}>
                    <Link to={"/categories"} className={"no-link-color"}>
                        <div className={"w-100 flex justify-center"}><AiOutlineSearch size={25} color={"#6C5DD4"}/>
                        </div>
                        <div>Qidirish</div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color"}>
                    <Link to={"/carts"} className={"no-link-color"}>
                        <div className={"w-100 flex justify-center"}><TiShoppingCart size={25} color={"#6C5DD4"}/>
                        </div>
                        <div>Savatcha</div>
                    </Link>
                </li>
                <li className={"p-2 text-sm my-color cursor-pointer"}>
                    <Link to={"/order"} className={"no-link-color"}>
                    <div className={"w-100 flex justify-center"}><BsFillBagHeartFill size={25}
                                                                                     color={"#6C5DD4"}/></div>
                    <div>Buyurtmalarim</div>
                    </Link>
                </li>
            </ul>
        )
    }

}
export const getRoleStorge = (): string | null => {
    return localStorage.getItem("role");
}
export const saveCartsLocalStorage = (product: IProduct) => {
    const cartsLocal: string | null | undefined = localStorage.getItem("carts");
    if (cartsLocal) {
        const cartsList: IAllCarts | null | undefined = JSON.parse(cartsLocal);
        if (cartsList?.carts?.some(book => book.book.id === product.id)) {
            cartsList.carts.forEach((book) => {
                    if (book.book.id === product.id) {
                        book.count = book.count + 1
                    }
                }
            );
            localStorage.setItem("carts", JSON.stringify(cartsList));
            console.log("buyoqqa keldi count", cartsList);
        } else {
            cartsList?.carts?.push({book: product, count: 1})
            localStorage.setItem("carts", JSON.stringify(cartsList));
            console.log("buyoqqa keldi push", cartsList?.carts);
        }
        toastSuccess("Savatchaga qo'shildi.")
    } else {
        console.log("buyoqqa keldi yangi");
        localStorage.setItem("carts", JSON.stringify({
            carts: [{
                book: product, count: 1,
            }]
        }));
        toastSuccess("Savatchaga qo'shildi.")
    }
}
export const getCartsLocalStorage = (): IAllCarts => {
    const cartsLocal = localStorage.getItem("carts");
    return JSON.parse(cartsLocal!);
}
export const deleteCartLocalStorage = (id: number | undefined) => {
    const cartsLocal = localStorage.getItem("carts");
    const cartsList: IAllCarts = JSON.parse(cartsLocal!);
    cartsList.carts.filter(book =>
        book.book.id !== id
    );
    localStorage.setItem("carts", JSON.stringify(cartsList));
}

export const minusCountCartLocalStorage = (count: number, id: undefined | number) => {
    const cartsLocal = localStorage.getItem("carts");
    const cartsList: IAllCarts = JSON.parse(cartsLocal!);
    if (count < 1) {
        cartsList.carts = cartsList.carts.filter(book => book.book.id !== id);
        console.log(cartsList)
    } else {
        cartsList.carts.forEach(book => {
                if (book.book.id === id) {
                    book.count = count
                }
            }
        );
    }
    localStorage.setItem("carts", JSON.stringify(cartsList));
    toastError("Savatchan o'chiriildi");
}
export const deleteCartsAll = () => {
    localStorage.removeItem("carts");
    toastError("Savatcha bo'shatildi");
}
export const savePhone = (number: string) => {
    localStorage.setItem("phone", number);
}
export const getPhoneLocalStorage = () => {
    return localStorage.getItem("phone");
}
export const pageNewurl = () => {
    window.history.pushState("object or string", "Title", "/orders");
    window.location.reload();
}
export const bookStateStyle = (name: string) => {

}

export enum ERole {
    Admin = "ROLE_ADMIN",
    User = "ROLE_USER"
}

const toastSuccess = (text: string | undefined | null) => {
    toast.success(text);
}
const toastError = (text: string | undefined | null) => {
    toast.error(text);
}
