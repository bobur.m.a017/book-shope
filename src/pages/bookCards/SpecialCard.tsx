import React, {FC, forwardRef, useState} from 'react';
import {SpecialProductDto} from "./SpecialProductDto";
import {TiShoppingCart} from "react-icons/ti";
import ModalOneBook from "../components/ModalOneBook";
import {IProduct} from "../fromAdmin/ProductDTO";
import {Carousel} from "react-bootstrap";
import imaa from "../../images/img.png";
import {imageUrl} from "../../allFunc/AllFuncs";
import {saveCartsLocalStorage} from "../functions/Functions";

interface specialCard {
    specialProductDto: IProduct
}

const SpecialCard = ({specialProductDto}: specialCard) => {
    const [oneCaed, setOneCaed] = useState({});
    const ref = React.useRef()
    const onClickedItem = () => {
        // setOneCaed(data);
        // @ts-ignore
        ref?.current?.handleShow();
    }


    return (
        <div className={"w-full"}>
            <div className={"special-card w-full position-relative"}>
                <div onClick={() => onClickedItem()}>
                    <div className={'discount position-absolute'}>{specialProductDto.discount} %</div>
                    <Carousel>
                        {
                            specialProductDto.attachments?.map((id) =>
                                <Carousel.Item key={id}>
                                    <div className={"special-card-img2 w-full h-[250px]"} style={{backgroundImage:`url(${imageUrl(id)})`}}>
                                    </div>
                                </Carousel.Item>)}
                    </Carousel>
                </div>
                <div className={"special-card-body px-3 py-2"}>
                    <div className={"my-scroll"}>
                        <h4 className={"flex min-w-[200px]"}>{specialProductDto.name}</h4>
                    </div>
                    <div className={"flex mt-2"}>
                        {
                            specialProductDto?.categories?.map((item, index) =>
                                <div key={index} className={"categories px-2 mx-1 text-sm"}>{item.name}</div>
                            )
                        }
                    </div>
                    <div className={"my-scroll-auto special-card-description mt-2"}>{specialProductDto.description}</div>
                    <div className={"special-card-price font-extrabold text-lg mt-2"}>{specialProductDto.author}</div>
                    <div className={"flex justify-between mt-5"}>
                        <div>
                            <button className={"my-bg-button flex items-center"} onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/><span
                                className={"mx-2 flex text-xs"}>Savatga qo‘shish</span></button>
                        </div>
                        <div className={"special-card-price position-relative"}>
                            <div className={"flex position-absolute text-rose-700"} style={{marginTop: -30}}>
                                <span>{specialProductDto.oldPrice}</span> <span className={"mx-1"}> so'm</span>
                            </div>
                            <div className={"line-price"}></div>
                            <div className={"flex font-semibold text-emerald-900"}>
                                {specialProductDto.price} so'm
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ModalOneBook specialProductDto={specialProductDto} ref={ref}/>
        </div>
    );
};

export default SpecialCard;
