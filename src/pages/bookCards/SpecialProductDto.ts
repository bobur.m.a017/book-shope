import imaa from "../../images/img.png";

export interface SpecialProductDto {
    id: number,
    header: string,
    title: string,
    image: any,
    author: string,
    categories: string[],
    price: number,
    discount: number,
    rating: number,
    status: string|null
}

export const specialProductDto = [{
    id: 1,
    header: "O‘tkan kunlar",
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
    image: imaa,
    author: "Abdulla Qodiriy",
    categories: ["TARIXIY", "TARIXIY", "TARIXIY"],
    price: 120000,
    discount: 50,
    rating: 4,
    status: null
}, {
    id: 2,
    header: "O‘tkan kunlar",
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
    image: imaa,
    author: "Abdulla Qodiriy",
    categories: ["TARIXIY", "TARIXIY", "TARIXIY"],
    price: 120000,
    discount: 50,
    rating: 4,
    status: "To'landi"
}, {
    id: 5,
    header: "O‘tkan kunlar",
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
    image: imaa,
    author: "Abdulla Qodiriy",
    categories: ["TARIXIY", "TARIXIY", "TARIXIY"],
    price: 120000,
    discount: 50,
    rating: 0,
    status: "Yetkazib berildi"
}, {
    id: 3,
    header: "O‘tkan kunlar",
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
    image: imaa,
    author: "Abdulla Qodiriy",
    categories: ["TARIXIY", "TARIXIY", "TARIXIY"],
    price: 120000,
    discount: 50,
    rating: 3,
    status: "Bekor qilindi"

}, {
    id: 4,
    header: "O‘tkan kunlar",
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
    image: imaa,
    author: "Abdulla Qodiriy",
    categories: ["TARIXIY", "TARIXIY", "TARIXIY"],
    price: 120000,
    discount: 50,
    rating: 4,
    status: null
},]
