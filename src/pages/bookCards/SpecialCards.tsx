import React, {useRef, useState} from 'react';
import {SpecialProductDto, specialProductDto} from "./SpecialProductDto";
import SpecialCard from "./SpecialCard";
import {Swiper, SwiperSlide} from "swiper/react";
import {A11y, Navigation, Pagination, Scrollbar} from "swiper";
import {useMediaQuery} from "react-responsive";
import ModalOneBook from "../components/ModalOneBook";
import {useGetProductsDiscountQuery, useGetProductsQuery} from "../../reducers/products/ProductsApi";
import {IProduct} from "../fromAdmin/ProductDTO";
import {useGetDiscountsQuery} from "../../reducers/discount/DiscountApi";

function SpecialCards() {
    const scrollRef = React.useRef<HTMLDivElement>(null)
    const mobile = useMediaQuery({
        query: '(max-width: 576px)'
    })
    const planshet = useMediaQuery({
        query: '(max-width: 768px)'
    })
    const desktop = useMediaQuery({
        query: '(max-width: 1024px)'
    })
    const scroll = (scrollOffset: number) => {
        // @ts-ignore
        scrollRef.current.scrollLeft += scrollOffset;
    };

    const books = useGetProductsDiscountQuery("");

    return (
        <div className={"w-full px-2"}>
            <div className={"my-scroll flex"} ref={scrollRef}>
                {books?.data?.map((item:IProduct, index:number) =>
                    <div key={index} className={"w-full sm:w-1/2 md:w-1/3 lg:w-1/3"}>
                        <SpecialCard specialProductDto={item}/>
                    </div>
                )}
            </div>
            <br/>
            <br/>
            <div className={"text-center"}>
                <div>
                    <button className={"mr-5"} onClick={() => scroll(-200)}>
                        <div className={"icon icon-left w-10 h-10"}></div>
                    </button>
                    <button className={"ml-5"} onClick={() => scroll(200)}>
                        <div className={"icon icon-right w-10 h-10"}></div>
                    </button>
                </div>
            </div>
        </div>
    );
}

export default SpecialCards;
