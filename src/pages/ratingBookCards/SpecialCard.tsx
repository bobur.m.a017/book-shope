import React, {FC, useState} from 'react';
import {TiShoppingCart} from "react-icons/ti";
import ModalOneBook from "../components/ModalOneBook";
import {SpecialProductDto} from "../bookCards/SpecialProductDto";
import {IProduct} from "../fromAdmin/ProductDTO";
import {Carousel} from "react-bootstrap";
import {imageUrl} from "../../allFunc/AllFuncs";
import {saveCartsLocalStorage} from "../functions/Functions";

interface specialCard {
    specialProductDto: IProduct
}

const SpecialCard = ({specialProductDto}: specialCard) => {
    const [oneCaed, setOneCaed] = useState({});
    const ref = React.useRef()
    const onClickedItem = () => {
        // setOneCaed(data);
        // @ts-ignore
        ref?.current?.handleShow();
    }
    return (
        <div className={"p-2 w-full"}>
            <div className={"sm:flex justify-between sm:items-center w-[100%] my-bg-card my-border h-full"}>
                <div onClick={() => onClickedItem()} className={"h-[50%] w-full sm:h-full sm:w-[50%] grid p-2 items-center"}>
                    <div>
                        <Carousel>
                            {
                                specialProductDto.attachments?.map((id) =>
                                    <Carousel.Item key={id}>
                                        <div className={"special-card-img2 w-full h-[250px] sm:h-[300px]"}
                                             style={{backgroundImage: `url(${imageUrl(id)})`}}>
                                        </div>
                                    </Carousel.Item>)}
                        </Carousel>
                    </div>
                </div>
                <div className={"special-card-body2 px-3 py-3 w-full h-[49%] sm:h-full sm:w-[50%]"}>
                    <div className={"my-scroll"}>
                        <h4 className={"flex min-w-[200px]"}>{specialProductDto.name}</h4>
                    </div>
                    <div className={"flex gap-2 mt-1"}>
                        {
                            specialProductDto?.categories?.map((item, index) =>
                                <div key={index} className={"categories px-2 text-xs text-center"}>{item.name}</div>
                            )
                        }
                    </div>
                    <div className={"my-scroll-auto special-card-description mt-2"}>{specialProductDto.description}</div>
                    <div className={"special-card-price font-extrabold text-lg mt-2"}>{specialProductDto.author}</div>
                    <div className={"flex justify-between md:block xl:flex mt-5"} style={{bottom: 0}}>
                        <button className={"my-bg-button md:hidden xl:flex  flex items-center"}
                                onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/><span
                            className={"mx-2 text-xs"}>Savatga qo‘shish</span></button>
                        <div className={"special-card-price position-relative"}>
                            <div className={"flex position-absolute text-rose-700"} style={{marginTop: -30}}>
                                <span>{specialProductDto.price}</span> <span className={"mx-1"}>so'm</span>
                            </div>
                            <div className={"line-price"}></div>
                            <div className={"flex font-semibold text-emerald-900"}>
                                {specialProductDto.price} so'm
                            </div>
                        </div>
                        <button className={"my-bg-button hidden md:flex xl:hidden  flex items-center"}
                                onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/><span
                            className={"mx-2 text-xs"}>Savatga qo‘shish</span></button>
                    </div>
                </div>
            </div>
            <ModalOneBook specialProductDto={specialProductDto} ref={ref}/>
        </div>
    );
}

export default SpecialCard;
