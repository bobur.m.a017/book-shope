import React, {useRef} from 'react';
import SpecialCard from "./SpecialCard";
import {useGetProductsQuery, useGetProductsRatingQuery} from "../../reducers/products/ProductsApi";
import {IProduct} from "../fromAdmin/ProductDTO";

function SpecialCards() {
    const scrollRef = React.useRef<HTMLDivElement>(null)
    const books = useGetProductsRatingQuery("")
    return (
        <div className={"px-2"}>
            <div className={"my-scroll-y2 my-scroll place-items-center grid grid-cols-1 sm:grid-cols-1 lg:grid-cols-2"} ref={scrollRef}>
                {books?.data?.map((item:IProduct, index:number) =>
                    <div key={index} className={"w-full  h-full"}>
                        <SpecialCard specialProductDto={item}/>
                    </div>
                )}
            </div>
            <br/>
            <br/>
        </div>
    );
}

export default SpecialCards;
