import React, {useEffect} from 'react';
import SpecialCards2 from "./allBookCards/SpecialCards";
import {useParams} from "react-router-dom";

function Product() {
   const category = useParams();
    useEffect(() => {
        console.log(category);
    }, []);

    return (
        <div>
            <SpecialCards2 id={category.id}/>
        </div>
    );
}

export default Product;
