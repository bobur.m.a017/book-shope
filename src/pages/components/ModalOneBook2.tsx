import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {SpecialProductDto} from "../bookCards/SpecialProductDto";
import {Modal} from "react-bootstrap";
import SpecialCard from "./SpecialCard";
import {IProduct} from "../fromAdmin/ProductDTO";

interface specialCard {
    specialProductDto: IProduct,
    show:true|false,
    handleClose:() => void|undefined
}

const ModalOneBook = forwardRef(({specialProductDto,show,handleClose}: specialCard) => {

    return (
        <Modal show={show} onHide={handleClose} size={'xl'}>
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                <div className={"my-scroll grid h-[530px] sm:h-full"}>
                    <SpecialCard specialProductDto={specialProductDto}/>
                </div>
            </Modal.Body>
        </Modal>
    );
});

export default ModalOneBook;
