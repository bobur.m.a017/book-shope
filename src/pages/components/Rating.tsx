import React from 'react';
import {BsStarFill} from "react-icons/bs";
import {FiStar} from "react-icons/fi";
import {ImStarEmpty} from "react-icons/im";

interface numRating {
    num: number | undefined
}

function Rating({num}: numRating) {
    const clickedRating = (num: number) => {
        alert(num)
    }
    return (
        <div className={""}>
            <div className={"flex"}>
                <div className={"mx-1 hover:cursor-pointer"} onClick={() => clickedRating(1)}>{num && num > 0 ?
                    <BsStarFill size={20} color={"#FA7701"}/> :
                    <ImStarEmpty size={20} color={"#FA7701"}/>}</div>
                <div className={"mx-1 hover:cursor-pointer"} onClick={() => clickedRating(2)}>{num && num > 1 ?
                    <BsStarFill size={20} color={"#FA7701"}/> :
                    <ImStarEmpty size={20} color={"#FA7701"}/>}</div>
                <div className={"mx-1  hover:cursor-pointer"} onClick={() => clickedRating(3)}>{num && num > 2 ?
                    <BsStarFill size={20} color={"#FA7701"}/> :
                    <ImStarEmpty size={20} color={"#FA7701"}/>}</div>
                <div className={"mx-1 hover:cursor-pointer"} onClick={() => clickedRating(4)}>{num && num > 3 ?
                    <BsStarFill size={20} color={"#FA7701"}/> :
                    <ImStarEmpty size={20} color={"#FA7701"}/>}</div>
                <div className={"mx-1 hover:cursor-pointer"} onClick={() => clickedRating(5)}>{num && num > 4 ?
                    <BsStarFill size={20} color={"#FA7701"}/> :
                    <ImStarEmpty size={20} color={"#FA7701"}/>}</div>
            </div>
            <div className={"text-xs text-center text-gray-500"}>Baholang</div>
        </div>
    );
}

export default Rating;
