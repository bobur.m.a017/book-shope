import React, {FC} from 'react';
import {TiShoppingCart} from "react-icons/ti";
import {SpecialProductDto} from "../bookCards/SpecialProductDto";
import Rating from "./Rating";
import {IProduct} from "../fromAdmin/ProductDTO";
import {Carousel} from "react-bootstrap";
import {imageUrl} from "../../allFunc/AllFuncs";
import {getRoleStorge, saveCartsLocalStorage} from "../functions/Functions";

interface specialCard {
    specialProductDto: IProduct
}

const SpecialCard: FC<specialCard> = ({specialProductDto}) => {
    return (
        <div className={"all p-0 sm:p-2 h-full min-w-[270px] h-full"}>
            <div className={"sm:flex justify-between my-bg-card my-border h-full"}>
                <Carousel>
                    {
                        specialProductDto.attachments?.map((id) =>
                            <Carousel.Item key={id}>
                                <img
                                    className="d-block w-full
                                        h-[250px] sm:h-[300px] lg:h-[350px]"
                                    src={imageUrl(id)}
                                    alt="First slide"
                                />

                            </Carousel.Item>)}
                </Carousel>
                <div className={"special-card-body2 px-3 py-2 w-full sm:w-[50%] md:w-[70%]"}>
                    <div className={"flex justify-between"}>
                        <div className={"my-scroll"}>
                            <h4 className={"flex min-w-[190px]"}>{specialProductDto.name}</h4>
                        </div>
                        <div><Rating num={specialProductDto.rating}/></div>
                    </div>
                    <div className={"flex gap-2 mt-2"}>
                        {
                            specialProductDto?.categories?.map((item, index) =>
                                <div key={index} className={"categories px-2 text-xs text-center"}>{item.name}</div>
                            )
                        }
                    </div>
                    <div className={"special-card-description mt-2"}>{specialProductDto.description}</div>
                    <div className={"special-card-price font-extrabold text-lg mt-2"}>{specialProductDto.author}</div>
                    <div className={"flex sm:block justify-between  md:flex mt-5"} style={{bottom: 0}}>
                        {getRoleStorge() !== "ROLE_ADMIN" ?  <button className={"my-bg-button hidden md:flex xl:flex  flex items-center"} onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/><span
                            className={"mx-2 text-xs"}>Savatga qo‘shish</span></button>:null}


                        <div className={"special-card-price position-relative"}>
                            <div className={"flex position-absolute text-rose-700"} style={{marginTop: -30}}>
                                <span>{specialProductDto.price}</span> <span className={"mx-1"}>so'm</span>
                            </div>
                            <div className={"line-price"}></div>

                            <div className={"flex font-semibold text-emerald-900"}>
                                {specialProductDto.price} so'm
                            </div>
                        </div>


                        {getRoleStorge() !== "ROLE_ADMIN" ? <button className={"my-bg-button md:hidden flex items-center mt-2"}
                                 onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/><span
                            className={"mx-2 text-xs"}>Savatga qo‘shish</span></button>: null}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SpecialCard;
