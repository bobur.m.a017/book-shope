import React, {forwardRef, useImperativeHandle, useState} from 'react';
import {SpecialProductDto} from "../bookCards/SpecialProductDto";
import {Modal} from "react-bootstrap";
import SpecialCard from "./SpecialCard";
import {IProduct} from "../fromAdmin/ProductDTO";

interface specialCard {
    specialProductDto: IProduct
}

const ModalOneBook = forwardRef(({specialProductDto}: specialCard, ref) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => {
        setShow(true);
    };
    useImperativeHandle(ref, () => ({
        handleShow: () => handleShow()
    }));
    return (
        <Modal show={show} onHide={handleClose} size={'xl'}>
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                <div className={"my-scroll grid h-full sm:h-full"}>
                    <SpecialCard specialProductDto={specialProductDto}/>
                </div>
            </Modal.Body>
        </Modal>
    );
});

export default ModalOneBook;
