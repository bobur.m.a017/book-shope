import React, {useEffect, useState} from 'react';
import {
    useDeleteDiscountMutation,
    useEditeDiscountMutation,
    useGetDiscountsQuery
} from "../reducers/discount/DiscountApi";
import {Accordion, Button, Form, Modal, Table} from "react-bootstrap";
import {IDiscount, IDiscountGet} from "../reducers/discount/DiscountDTO";
import DropdownCustom from "./DropdownCustom";
import {IProduct} from "./fromAdmin/ProductDTO";
import {toast} from "react-toastify";
import {useGetProductsAdminQuery} from "../reducers/products/ProductsApi";
import {AiFillLock, AiFillUnlock, AiTwotoneDelete} from "react-icons/ai";

function Discounts() {
    const [show, setShow] = useState(false);
    const [discountState, setDiscountState] = useState<IDiscountGet | null>();
    const [num, setNum] = useState(0);
    const handleClose = () => setShow(false);
    const handleShow = (num: number, data: null | IDiscountGet) => {

        setDiscountState(data);
        setShow(true)
    };
    const discounts = useGetDiscountsQuery("");
    const bookApi = useGetProductsAdminQuery("")
    const [editeDiscount, editeRes] = useEditeDiscountMutation()
    const [deleteDiscount, deleteRes] = useDeleteDiscountMutation()
    useEffect(() => {

    }, [discounts]);

    const getBookDropdown = (data: IProduct | any) => {
        if (discountState?.books?.some(item => item.id === data.id)) {
            toast.error("Bu kitob qo'shilgan");
        } else {
            if (discountState?.books) {
                let list: [] | any = [...discountState?.books];
                list?.push(data)
                setDiscountState({...discountState, books: list})
            }
        }
    }
    const deleteBooks = (data: IProduct) => {
        setDiscountState({...discountState, books: discountState?.books?.filter(item => item.id !== data.id)});
    }

    const submitEdite = (e: React.FormEvent) => {
        e.preventDefault();
        editeDiscount({...discountState, books: discountState?.books?.map(item => item.id)} as IDiscount);
        handleClose();
    }
    const renderEditeDiscount = () => {
        return (
            <Form onSubmit={submitEdite}>
                <Modal.Header closeButton>
                    <Modal.Title className={""}>
                        <span> {discountState?.percentage} % lik chegirma </span>
                        <span><Form.Control
                            name={"percentage"} size={"sm"} value={discountState?.percentage}
                            onChange={(e) => setDiscountState({
                                ...discountState,
                                percentage: parseInt(e.target.value)
                            })}/></span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <DropdownCustom list={bookApi.data} getItem={getBookDropdown}/>
                    <Table size={"sm"}>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Kitob nomi</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            discountState?.books?.map((book, index2) =>
                                <tr key={index2}>
                                    <td>{index2 + 1}</td>
                                    <td>{book.name}</td>
                                    <td onClick={() => deleteBooks(book)} className={"cursor-pointer"}>
                                        <AiTwotoneDelete
                                            color={'red'} size={25}/>
                                    </td>
                                </tr>
                            )
                        }
                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Ortga
                    </Button>
                    <Button variant="primary" type={'submit'}>
                        O'zgartirish
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }

    return (
        <div className={"px-2"}>
            {
                discounts.data?.map((item, index: number) =>
                    <Accordion key={index}>
                        <Accordion.Item eventKey={`${index}`}>
                            <Accordion.Header className={"w-full"}>
                                <div className={"w-full flex justify-between"}>
                                    <div>
                                        Kitoblar soni <span className={"my-color"}>{item.books?.length}</span>
                                    </div>
                                    <div> Tugash sanasi <span
                                        className={"my-color"}> {new Date(item.validityPeriod!).toLocaleDateString()}</span>
                                    </div>
                                    <div>
                                        foizi <span className={"my-color"}> {item.percentage}</span>
                                    </div>
                                    <div>
                                        Holati {item.state ? <span className={"text-green-600"}> Faol</span> :
                                        <span className={"text-rose-700"}>No faol</span>}
                                    </div>
                                </div>
                            </Accordion.Header>
                            <Accordion.Body>
                                <div className={"flex items-center w-full"}>
                                    <button className={"my-bg-button"}
                                            onClick={() => handleShow(0, item)}>
                                        O'zgartirish
                                    </button>
                                    <button className={"my-bg-button mx-2"} onClick={() => deleteDiscount(item as IDiscount)}><AiTwotoneDelete
                                        color={'red'} size={25}/>
                                    </button>
                                    <button className={"my-w-button"}>{!item.state ?
                                        <AiFillLock size={25} color={'red'}/> :
                                        <AiFillUnlock color={"green"} size={25}/>}</button>
                                </div>
                                <Table size={"sm"}>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kitob nomi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        item.books?.map((book, index2) =>
                                            <tr key={index2}>
                                                <td>{index2 + 1}</td>
                                                <td>{book.name}</td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                )
            }
            <Modal show={show} onHide={handleClose}>
                {num === 0 ? renderEditeDiscount() : null}
            </Modal>
        </div>
    );
}

export default Discounts;
