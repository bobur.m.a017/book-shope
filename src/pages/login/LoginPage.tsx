import React, {useEffect, useState} from 'react';
import './style.scss'
import {useLoginUserMutation} from "../../reducers/posts/UsersApi";
import {saveToken} from "../../store/axiosApi";
import {useNavigate} from "react-router-dom";
import {ERole} from "../functions/Functions";

function LoginPage() {
    const history = useNavigate();
    const [data2, setData] = useState({username: '', password: ''});
    const [loginUser, userLogin] = useLoginUserMutation()
    useEffect(() => {
        if (userLogin.data) {
            saveToken(userLogin.data);
            if (userLogin?.data?.user_role === ERole.Admin) {
                history("/admin");
            } else {
                history("/");
            }
        }
    }, [userLogin]);

    const loginSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const qs = require('qs');
        loginUser(qs.stringify(data2))
    }
    return (
        <div className={"login"}>
            <div className={"loginUp w-full h-[100vh] "}>

            </div>
            <div className={"flex p-2 h-[80vh]  justify-center items-center"}>
                <div className={"h-[50vh] w-full inLogin2 flex justify-center"}>
                    <div className={"w-full h-full sm:w-[50vh] lg:w-1/3 inLogin flex justify-center items-center"}>
                        <div>
                            <div className={"flex justify-center mb-2"}>
                                <div className={"icon-logo-yk w-24 h-20 my-radius-buttons text-center"}></div>
                            </div>
                            <form onSubmit={loginSubmit}>
                                <input type="text" name={"username"}
                                       className={"my-border my-radius-buttons  text-center"}
                                       placeholder={"Login"} value={data2.username}
                                       onChange={(e) => setData({...data2, username: e.target.value})}/>
                                <br/>
                                <input type="password" name={"password"}
                                       className={"my-border my-radius-buttons my-2 text-center"}
                                       placeholder={"Parol"} value={data2.password}
                                       onChange={(e) => setData({...data2, password: e.target.value})}/>
                                <br/>
                                <div className={"flex justify-center"}>
                                    <button className={"my-bg-button px-2"} type={"submit"}>Kirish</button>
                                </div>
                                <div className={"flex justify-center"}>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LoginPage;
