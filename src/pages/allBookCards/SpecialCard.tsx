import React, {FC, useState} from 'react';
import {TiShoppingCart} from "react-icons/ti";
import {FiPlus} from "react-icons/fi";
import ModalOneBook from "../components/ModalOneBook";
import {IProduct} from "../fromAdmin/ProductDTO";
import {Carousel} from "react-bootstrap";
import {imageUrl} from "../../allFunc/AllFuncs";
import {minusCountCartLocalStorage, saveCartsLocalStorage} from "../functions/Functions";
import {AiOutlineMinusCircle, AiOutlinePlusCircle} from "react-icons/ai";
import * as url from "url";

interface specialCard {
    specialProductDto: IProduct,
    count?: number,
    getCartsFun?: () => void
}

const SpecialCard = ({specialProductDto, count, getCartsFun}: specialCard) => {
    const [oneCard, setOneCard] = useState({});
    const [district, setDistrict] = useState({});
    const [pathName, setPathName] = useState(window.location.href);
    const ref = React.useRef();
    const onClickedItem = () => {
        // setOneCaed(data);
        // @ts-ignore
        ref?.current?.handleShow();
    }
    const geyDistrict = (id: number) => {

    }
    const saveCarts = () => {
        saveCartsLocalStorage(specialProductDto);
        if (getCartsFun)
            getCartsFun();
    }
    const deleteCarts = (counts: number | undefined) => {
        if (counts && counts >= 1) {
            minusCountCartLocalStorage(counts - 1, specialProductDto.id);
            if (getCartsFun)
                getCartsFun();
        }
    }
    return (
        <div>
            <div className={"special-card w-[250px] h-full"}>
                <div className={"pt-3 px-3 my-bg-card"}>
                    <Carousel>
                        {
                            specialProductDto.attachments?.map((id) =>
                                <Carousel.Item key={id}>
                                    <div className={"special-card-img2 w-full h-[250px]"}
                                         style={{backgroundImage: `url(${imageUrl(id)})`}}>
                                    </div>

                                </Carousel.Item>)}
                    </Carousel>
                </div>
                <div className={"special-card-body px-3 py-2 h-[250px] position-relative"}>
                    <div className={"my-scroll"}>
                        <h4 className={"flex min-w-[190px]  my-scroll-auto"}>{specialProductDto.name}</h4>
                    </div>
                    <div className={"my-scroll-auto special-card-description mt-2"}>{specialProductDto.description}</div>
                    <div className={"position-absolute bottom-2 w-full"}>
                    <div
                        className={"special-card-price font-extrabold text-lg mt-2 my-scroll-auto"}>{specialProductDto.author}</div>
                        <div className={"flex justify-between  mt-7"}>
                            {pathName.endsWith("/carts") ? <div className={"text-center"}>
                                <div onClick={() => saveCarts()} className={"cursor-pointer"}><AiOutlinePlusCircle/>
                                </div>
                                <div>
                                    {count}
                                </div>
                                <div onClick={() => deleteCarts(count)} className={"cursor-pointer"}>
                                    <AiOutlineMinusCircle/>
                                </div>
                            </div> : <div>
                                <button className={"my-bg-button flex px-2 items-center"}
                                        onClick={() => saveCartsLocalStorage(specialProductDto)}><TiShoppingCart/>
                                    <FiPlus/>
                                </button>
                            </div>}
                            <div className={"special-card-price position-relative mr-8"}>
                                {specialProductDto.oldPrice ? <div className={"flex position-absolute text-rose-700"} style={{marginTop: -30}}>
                                    <span>{specialProductDto.oldPrice}</span> <span className={"mx-1"}> so'm</span>
                                </div>:null}
                                {specialProductDto.oldPrice ? <div className={"line-price"}></div>:null}
                                <div className={"flex text-emerald-900"}>
                                    {specialProductDto.price} so'm
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ModalOneBook specialProductDto={specialProductDto} ref={ref}/>
        </div>
    );
}

export default SpecialCard;
