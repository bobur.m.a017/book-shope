import React, {useRef} from 'react';
import SpecialCard from "./SpecialCard";
import {useMediaQuery} from "react-responsive";
import {useGetProductsQuery} from "../../reducers/products/ProductsApi";
import {IProduct} from "../fromAdmin/ProductDTO";

interface ICategory {
    id?:string
}
function SpecialCards({id}:ICategory) {
    const scrollRef = React.useRef<HTMLDivElement>(null)
    const mobile = useMediaQuery({
        query: '(max-width: 576px)'
    })
    const planshet = useMediaQuery({
        query: '(max-width: 768px)'
    })
    const desktop = useMediaQuery({
        query: '(max-width: 1024px)'
    })
    const scroll = (scrollOffset:number) => {
        // @ts-ignore
        scrollRef.current.scrollLeft += scrollOffset;
    };

const books =  useGetProductsQuery(id)

    return (
        <div className={"px-2 drop-shadow-lg shadow-black"}>
            <div className={"place-items-center grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5"} ref={scrollRef}>
                {books?.data?.map((item:IProduct, index:number) =>
                    <div key={index}>
                        <SpecialCard specialProductDto={item}/>
                    </div>
                )}
            </div>
        </div>
    );
}

export default SpecialCards;
