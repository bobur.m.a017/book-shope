import React, {useEffect, useState} from 'react';
import "./styles/index2.scss"
import {Link, Route, Routes, useNavigate} from "react-router-dom";
import Home from "./pages/Home";
import 'swiper/swiper-bundle.min.css';
import 'swiper/css';
import {Dropdown} from "react-bootstrap";
import {AiOutlineAppstore, AiOutlineDown, AiOutlineSearch} from "react-icons/ai";
import {TiShoppingCart} from "react-icons/ti";
import Carts from "./pages/Carts";
import Orders from "./pages/Orders";
import Orders2 from "./pages/fromAdmin/Orders";
import {ERole, getLinks, getRoleStorge} from "./pages/functions/Functions";
import Categories from "./pages/fromAdmin/Categories";
import Products from "./pages/fromAdmin/Products";
import LoginPage from "./pages/login/LoginPage";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import IsLoading from "./pages/IsLoading";
import {useAppSelector} from "./hooks/Hooks";
import {useGetCategoriesQuery} from "./reducers/categories/CategoriesApi";
import Product from "./pages/Product";
import Discounts from "./pages/Discounts";
import {useGetProductsSearchQuery} from "./reducers/products/ProductsApi";
import {BiCategory} from "react-icons/bi";
import Admin from "./pages/fromAdmin/Admin";
import CheckAuth from "./pages/fromAdmin/CheckAuth";
import UserLogin from "./pages/UserLogin";

function App() {
    const history = useNavigate();
    const [params, setParams] = useState("");
    const dropdownMenuStyle = {
        backgroundColor: '#fff',
        borderStyle: 'solid',
        borderColor: '#6C5DD4',
        color: "#6C5DD4",
        borderBottomLeftRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        height: "100%",
    }
    const loading = useAppSelector(state => state.loading.loading);
    const category = useGetCategoriesQuery('')
    const booksSearch = useGetProductsSearchQuery({params: {search: params}});
    useEffect(() => {
        booksSearch.refetch();
    }, [params]);

    return (
        <div className={"all"}>
            <IsLoading loading={loading}/>
            <nav
                className={"sticky-top top-0 bg-white shadow hidden md:flex navbar-style flex md:px-8 xs:px-5 justify-between"}>

                <div className={"flex w-[115px] h-[75px] icon-logo-yk mr-8 cursor-pointer"}
                     onClick={() => history("/")}>
                </div>

                {getRoleStorge() !== ERole.Admin ? <div className={"flex justify-around lg:w-[60%] md:w-[80%]"}>
                    <div className={"flex justify-center py-3"}>
                        <div className={"flex  w-[100%]"}>
                            <Dropdown>
                                <Dropdown.Toggle bsPrefix={"'dropdown-toggle'"} style={dropdownMenuStyle}>
                                    <div className={"flex items-center"}>
                                        <AiOutlineAppstore size={25}/> <span className={"mx-1"}>Menyu</span>
                                        <AiOutlineDown/>
                                    </div>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    {
                                        category?.data?.map((category) =>
                                            <Dropdown.Item
                                                key={category.id}>
                                                <Link to={"/product/" + category.id} className={"no-link-color"}>
                                                    {category.name}
                                                </Link>
                                            </Dropdown.Item>
                                        )
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                            <div className={"input-search  lg:w-[70%] position-relative"}>
                                <input type="text" className={"w-full h-full"} value={params}
                                       onChange={e => setParams(e.target.value)}/>
                                <br/>
                                <div className={"position-absolute w-full"}>
                                    {
                                        params ? booksSearch.data?.map((book, index) =>
                                            <div key={index} className={"w-full search-list"}
                                                 style={{backgroundColor: 'white'}}>
                                                {index + 1} {book.name}
                                            </div>
                                        ) : null
                                    }
                                </div>
                            </div>
                            <div className={"my-border flex items-center rounded-tr-md rounded-br-md px-2"}>
                                <AiOutlineSearch size={25} color={"#6C5DD4"}/>
                            </div>
                        </div>
                    </div>
                    <div className={"flex justify-around items-center"}>
                        <Link to={"/categories"} className={"no-link-color my-border-color rounded-xl p-2 flex"}>
                            <div className={"w-100 flex justify-center"}><BiCategory size={25} color={"#6C5DD4"}/>
                            </div>
                            <div>Bo'limlar</div>
                        </Link>
                        {getRoleStorge() === ERole.Admin ? <button className={"my-bg-button px-2 mx-2"}>
                            <Link to={"/admin/orders"}
                                  className={"no-link text-white"}> Buyurtmalar </Link>
                        </button> : <button className={"my-bg-button px-2 mx-2"}>
                            <Link to={"/order"}
                                  className={"no-link text-white"}> Mening buyurtmalarim</Link>
                        </button>}
                        <Link to={"/carts"} className={"no-link-color"}> <TiShoppingCart size={30}
                                                                                         color={"#6C5DD4"}/></Link>
                    </div>
                </div> : null}
            </nav>
            <div className={"mt-0 mb-10 md:mb-0 md:mt-20 "}>
                <Routes>
                    <Route path={"/"} element={<Home/>}/>
                    <Route path={"/carts"} element={<Carts/>}/>
                    <Route path={"/order"} element={
                        <UserLogin>
                            <Orders/>
                        </UserLogin>
                    }/>
                    <Route path={"/categories"} element={<Categories/>}/>
                    <Route path={"/product/:id"} element={<Product/>}/>
                    <Route path={"/product/"} element={<Product/>}/>
                    <Route path={"/login"} element={<LoginPage/>}/>
                    <Route path={"/admin/*"} element={
                        <CheckAuth>
                            <Admin/>
                        </CheckAuth>
                    }/>
                </Routes>
            </div>
            <nav className={"position-fixed md:hidden z-10 bottom-0 w-100 bg-white shadow-2xl shadow-black"}>
                {
                    getLinks(getRoleStorge())
                }
            </nav>
            <ToastContainer/>
        </div>
    );
}

export default App;
