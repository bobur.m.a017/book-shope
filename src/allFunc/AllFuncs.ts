export const baseUrl = () => {
    // return "http://192.168.43.241:8888/api"
    // return "http://localhost:8888/api"
    return "https://yoshkitobxon.uz/api"
    // return "http://95.130.227.104:8888/api"
}
export const imageUrl = (num: number) => {
    return baseUrl() + "/attachment/" + num
}
